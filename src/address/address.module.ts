import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from 'src/users/users.module';
import { AddressController } from './address.controller';
import { Address } from './address.entity';
import { AddressService } from './address.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Address]),
    forwardRef(() => UsersModule),
  ],
  exports: [AddressService],
  controllers: [AddressController],
  providers: [AddressService]
})
export class AddressModule {}
