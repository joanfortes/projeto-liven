import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CrudRequest } from '@nestjsx/crud';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { User } from 'src/users/users.entity';
import { Repository } from 'typeorm';
import { Address } from './address.entity';
import { CreateAddressDTO } from './dto/create-address.dto';
import { UpdateAddressDTO } from './dto/update-address.dto';

@Injectable()
export class AddressService extends TypeOrmCrudService<Address>{
    constructor(
      @InjectRepository(Address) repo: Repository<Address>
    ) {
      super(repo);
    }

    async createOneAddressWithUser(dto: CreateAddressDTO,user: User,req:CrudRequest) {
      const address = await this.repo.findOne({...dto});
      if(address){
        throw new BadRequestException('O endereço informado já existe');
      } else {
        return super.createOne(req,this.repo.create({...dto,user:user}));
      }
    }

    async updateOneAddress(dto: UpdateAddressDTO, id: number,user: User) {
      await this.repo.update(id,{...dto});
      return this.repo.manager.findOne(User,{
        where: {id:user.id},
        relations:['addresses'],
      });
    }

    async deleteOneAddress(req: CrudRequest) {
      return super.deleteOne(req);
    }

}
