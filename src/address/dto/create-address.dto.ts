import { baseTimeStampFields } from "src/common/base-timestamp.entity";
import { ExcludeType } from "src/helpers/exclude-type.helper";
import { Address } from "../address.entity";

export class CreateAddressDTO extends ExcludeType(Address,[...baseTimeStampFields]) {}