import { Address } from 'src/address/address.entity';
import { GetUserDTO } from 'src/users/dto/get-usuario.dto';

export class GetAddressDTO {
    user: GetUserDTO;

    Address: Address | Address[];
}