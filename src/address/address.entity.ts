import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsOptional } from "class-validator";
import { BaseTimeStampEntity } from "src/common/base-timestamp.entity";
import { MAX_CITY_NAME_LENGTH, MAX_COMPLEMENT_NAME_LENGTH, MAX_NEIGHBORHOOD_NAME_LENGTH, MAX_STREET_NAME_LENGTH, MIN_TEXT_LENGTH, ZIP_CODE_LENGTH } from "src/config/constraints";
import { CustomUniqueErrorMessage } from "src/helpers/decorators.helper";
import { User } from "src/users/users.entity";
import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, Unique } from "typeorm";
import { BrazilianStates, Country } from "./address.interface";
@Entity('address')
@Unique('unique_address',[
	'zipCode',
	'streetName',
	'neighborhood',
	'houseNumber',
	'complement',
	'city',
	'state',
	'country'
])
@CustomUniqueErrorMessage({constraintName: 'unique_address',message:'Esse endereço já existe'})
export class Address extends BaseTimeStampEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column('varchar', { length: ZIP_CODE_LENGTH })
	@IsNotEmpty()
	@ApiProperty({
		type: String,
		minLength: ZIP_CODE_LENGTH,
		maxLength: ZIP_CODE_LENGTH,
	})
	zipCode: string;

	@Column({ type: 'varchar', length: MAX_STREET_NAME_LENGTH, nullable: true })
	@ApiProperty({
		type: String,
		minLength: MIN_TEXT_LENGTH,
		maxLength: MAX_STREET_NAME_LENGTH,
	})
	@IsNotEmpty()
	streetName: string;

	@Column({ type: 'varchar', length: MAX_NEIGHBORHOOD_NAME_LENGTH, nullable: true })
	@ApiProperty({
		type: String,
		minLength: MIN_TEXT_LENGTH,
		maxLength: MAX_NEIGHBORHOOD_NAME_LENGTH,
	})
	neighborhood: string;

	@Column('numeric', { unsigned: true })
	@IsNotEmpty()
	@ApiProperty()
	houseNumber: number;

	@Column({ type: 'varchar', length: MAX_COMPLEMENT_NAME_LENGTH, nullable: true })
	@ApiProperty({
		required: false,
		type: String,
		minLength: MIN_TEXT_LENGTH,
		maxLength: MAX_COMPLEMENT_NAME_LENGTH,
	})
	@IsOptional({ always: true })
	complement: string;

	@Column({ type: 'varchar', length: MAX_CITY_NAME_LENGTH, nullable: true })
	@ApiProperty({
		type: String,
		minLength: MIN_TEXT_LENGTH,
		maxLength: MAX_CITY_NAME_LENGTH,
	})
	@IsNotEmpty()
	city: string;

	@Column({ type: 'enum', enum: BrazilianStates, nullable: true })
	@ApiProperty({ type: BrazilianStates, enum: BrazilianStates })
	@IsNotEmpty()
	state: BrazilianStates;

	@Column({
		type: 'enum',
		enum: Country,
	})
	@ApiProperty({ type: Country, enum: Country })
	@IsNotEmpty()
	country: Country;


    @ManyToOne(() => User, (users) => users.addresses, {nullable: false})
	@JoinColumn([{name:'id_usuario',referencedColumnName: 'id'}])
	user: User;
}