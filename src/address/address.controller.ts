import { Controller, Param, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Crud, CrudAuth, CrudController, CrudRequest, Override, ParsedBody, ParsedRequest } from '@nestjsx/crud';
import { CurrentUser } from 'src/helpers/decorators.helper';
import { User } from 'src/users/users.entity';
import { Address } from './address.entity';
import { AddressService } from './address.service';
import { CreateAddressDTO } from './dto/create-address.dto';
import { UpdateAddressDTO } from './dto/update-address.dto';

@Crud({
  model:{
    type: Address
  },
  dto:{
    create:CreateAddressDTO,
    update:UpdateAddressDTO
  },
  query:{
    join:{
      user: {
        eager:true,
        required: true,
      },
    }
  },
  routes:{
    only:['createOneBase','updateOneBase','deleteOneBase','getManyBase']
  }
})
@CrudAuth({
  persist: (usuario: User) =>{
    return usuario;
  }
})
@UseGuards(AuthGuard())
@ApiBearerAuth('accessToken')
@ApiTags('Endereços')
@Controller('/user/address')
export class AddressController implements CrudController<Address> {
  constructor(public service: AddressService) {}

  @Override('createOneBase')
  async getOne(
    @ParsedBody() dto: CreateAddressDTO,
    @CurrentUser() user: User,
    @ParsedRequest() req: CrudRequest
  ) {
    return this.service.createOneAddressWithUser(dto,user,req);
  }

  @Override('updateOneBase')
  async updateOne(
    @ParsedBody() dto: UpdateAddressDTO,
    @Param('id') idAddress: number,
    @CurrentUser() user: User,
  ) {
    return this.service.updateOneAddress(dto,idAddress,user);
  }

  @Override('deleteOneBase')
  async deleteOne(
    @ParsedRequest() req: CrudRequest
  ) {
    return this.service.deleteOneAddress(req);
  }
}
