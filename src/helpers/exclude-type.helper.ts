import { Type } from "@nestjs/common";
import { ApiHideProperty, OmitType } from "@nestjs/swagger";
import { Exclude } from "class-transformer";

/**
 * Decorator que cria uma classe com os campos Escondidos na documentação
 * e exlcuidos, com [Exclude].
 */
export function ExcludeType<T, K extends keyof T>(classRef: Type<T>, fields: readonly K[]) {
  const type = OmitType(classRef, fields);
  fields.forEach((field) => {
    ApiHideProperty()(type.prototype, String(field));
    Exclude({ toClassOnly: true })(type.prototype, String(field));
  });
  return type;
}3