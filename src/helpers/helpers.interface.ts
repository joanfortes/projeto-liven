export interface CustomUniqueMessageOptions {
    /**
     * Mensagem a ser mostrado quando der erro de unique nesse campo/classe
     */
    message: string
    /**
     * Só é fornecido quando é usado em cima de uma classe
     * (ou seja, em conjunto com @Unique)
     * Como pode ter mais que um @Unique definido na classe,
     * deve fornecer o nome dado ao constraint.
     * (@Unique permite nomear a constraint)
     */
    constraintName?: string;
}

export interface CustomFKMessageOptions {
    /**
     * Mensagem a ser mostrado quando der erro de unique nesse campo/classe
     */
    message: string
    /**
     * Campos que estão envolvidos na FK, deve bater EXATAMENTE
     */
    fields: string[];
}

export interface CustomConstraintOptions extends CustomFKMessageOptions {
    tableName?: string;
    target: any;
}
  
export interface CustomUniqueConstraintOptions extends CustomUniqueMessageOptions {
    tableName?: string;
    target: any;
    propertyName: string | symbol;
    fields: string[];
}
  
export interface CustomConstraints {
    uniques: CustomUniqueConstraintOptions[];
    fks: CustomConstraintOptions[];
}

const customConstraints: CustomConstraints = {
    uniques: new Array<CustomUniqueConstraintOptions>(),
    fks: new Array<CustomConstraintOptions>(),
  };
  
export const getCustomConstraints = () => customConstraints;