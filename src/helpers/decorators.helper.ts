import { createParamDecorator, ExecutionContext } from "@nestjs/common";
import { CustomUniqueMessageOptions, getCustomConstraints } from "./helpers.interface";

export const CurrentUser = createParamDecorator((data, ctx: ExecutionContext) => {
  const req = ctx.switchToHttp().getRequest();
  return req.user;
});

export const CustomUniqueErrorMessage = (options: CustomUniqueMessageOptions): ClassDecorator & PropertyDecorator => {
  return function (target: Record<string, unknown> | Function, propertyName?: string | symbol) {
    if (!propertyName && !options.constraintName)
      throw 'Deve ser fornecido um nome do constraint quando usado na classe, leia a docs das opções!';

    getCustomConstraints().uniques.push({
      ...options,
      // Se não tiver propriedade, é decorator de classe, joga classe direto
      target: (!propertyName) ? target : target.constructor,
      propertyName,
      fields: null,
    })
  };
}