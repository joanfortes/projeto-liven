import { MigrationInterface, QueryRunner } from "typeorm";

export class createDatabase1637487572792 implements MigrationInterface {
    name = 'createDatabase1637487572792'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "user" ("created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP WITH TIME ZONE, "id" SERIAL NOT NULL, "name" character varying(32) NOT NULL, "email" character varying NOT NULL, "password" character varying NOT NULL, CONSTRAINT "UQ_e12875dfb3b1d92d7d7c5377e22" UNIQUE ("email"), CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TYPE "public"."address_state_enum" AS ENUM('AC', 'AL', 'AP', 'AM', 'BA', 'CE', 'DF', 'ES', 'GO', 'MA', 'MT', 'MS', 'MG', 'PA', 'PB', 'PR', 'PE', 'PI', 'RJ', 'RN', 'RS', 'RO', 'RR', 'SC', 'SP', 'SE', 'TO')`);
        await queryRunner.query(`CREATE TYPE "public"."address_country_enum" AS ENUM('BRA')`);
        await queryRunner.query(`CREATE TABLE "address" ("created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP WITH TIME ZONE, "id" SERIAL NOT NULL, "zipCode" character varying(12) NOT NULL, "streetName" character varying(128), "neighborhood" character varying(128), "houseNumber" numeric NOT NULL, "complement" character varying(128), "city" character varying(128), "state" "public"."address_state_enum", "country" "public"."address_country_enum" NOT NULL, "id_usuario" integer NOT NULL, CONSTRAINT "UQ_d3d7f833fef6295c6cc3e9497f1" UNIQUE ("zipCode", "streetName", "neighborhood", "houseNumber", "complement", "city", "state", "country"), CONSTRAINT "PK_d92de1f82754668b5f5f5dd4fd5" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "address" ADD CONSTRAINT "FK_0a8229e501c312d3ba0625cc83c" FOREIGN KEY ("id_usuario") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "address" DROP CONSTRAINT "FK_0a8229e501c312d3ba0625cc83c"`);
        await queryRunner.query(`DROP TABLE "address"`);
        await queryRunner.query(`DROP TYPE "public"."address_country_enum"`);
        await queryRunner.query(`DROP TYPE "public"."address_state_enum"`);
        await queryRunner.query(`DROP TABLE "user"`);
    }

}
