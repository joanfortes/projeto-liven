import { MigrationInterface, QueryRunner } from "typeorm";

export class UpdateAddress1637489317032 implements MigrationInterface {
    name = 'UpdateAddress1637489317032'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "address" DROP CONSTRAINT "UQ_d3d7f833fef6295c6cc3e9497f1"`);
        await queryRunner.query(`ALTER TABLE "address" ADD CONSTRAINT "unique_address" UNIQUE ("zipCode", "streetName", "neighborhood", "houseNumber", "complement", "city", "state", "country")`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "address" DROP CONSTRAINT "unique_address"`);
        await queryRunner.query(`ALTER TABLE "address" ADD CONSTRAINT "UQ_d3d7f833fef6295c6cc3e9497f1" UNIQUE ("zipCode", "streetName", "neighborhood", "houseNumber", "complement", "city", "state", "country")`);
    }

}
