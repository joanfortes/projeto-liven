import {MigrationInterface, QueryRunner} from "typeorm";

export class endingAllEntities1637522171116 implements MigrationInterface {
    name = 'endingAllEntities1637522171116'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" ADD "salt" character varying NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "salt"`);
    }

}
