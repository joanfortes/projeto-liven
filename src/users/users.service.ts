import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { CrudRequest } from '@nestjsx/crud';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import * as bcrypt from 'bcrypt';
import { plainToClass } from 'class-transformer';
import { Address } from 'src/address/address.entity';
import { AuthCredentialsDto, SingInDTO } from 'src/users/dto/auth-credentials.dto';
import { GetUserDTO } from 'src/users/dto/get-usuario.dto';
import { JwtPayload } from 'src/users/jwt-utilities/jwt-payload.interface';
import { Repository } from 'typeorm';
import { CreateUserDTO } from './dto/create-usuario.dto';
import { UpdateUserDTO } from './dto/update-usuario.dto';
import { User } from './users.entity';

@Injectable()
export class UsersService extends TypeOrmCrudService<User>{

  constructor(
      @InjectRepository(User) repo: Repository<User>,
      private jwtService: JwtService,
  ) {
      super(repo);
  }

  async updateOneBase(req: CrudRequest,dto: UpdateUserDTO) {
    return super.updateOne(req,{...dto});
  }

  async getOneUser(req: CrudRequest):Promise<GetUserDTO> {
    return plainToClass(GetUserDTO,await super.getOne(req));;
  }

  async deleteOneUser(user:User,req: CrudRequest) {
    await this.repo.manager.delete(Address,{user:user.id});
    return super.deleteOne(req);
  }

  async signUp(dto: CreateUserDTO): Promise<GetUserDTO> {
    const usuario = await this.repo.findOne({name: dto.name});
    if (usuario) {
      throw new UnauthorizedException('Usuário já existe');
    }
    const salt = await bcrypt.genSalt();
    const pass = await bcrypt.hash(dto.password, salt);
    const user = await this.repo.save({...dto, salt:salt,password: pass});
    return plainToClass(GetUserDTO,user);
  }

  async signIn(dto: AuthCredentialsDto): Promise<SingInDTO> {
    const user = await this.repo.findOne({name:dto.name});
    const validPassword = await user.validatePassword(dto.password);

    if (!validPassword) {
      throw new UnauthorizedException('Invalid credentials');
    }

    const payload: JwtPayload = { username: dto.name };
    const accessToken = this.jwtService.sign(payload);
    const userSemDadosSensiveis = plainToClass(GetUserDTO,user);
    const userLogadoSucesso: SingInDTO = plainToClass(SingInDTO,{accessToken: accessToken,user: userSemDadosSensiveis});

    return userLogadoSucesso;
  }
}
