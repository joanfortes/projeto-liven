import { IsOptional } from "class-validator";
import { baseTimeStampFields } from "src/common/base-timestamp.entity";
import { ExcludeType } from "src/helpers/exclude-type.helper";
import { User } from "../users.entity";

export class UpdateUserDTO extends ExcludeType(User,['addresses','password','id',...baseTimeStampFields]) {
    @IsOptional()
    name: string;

    @IsOptional()
    email: string
}