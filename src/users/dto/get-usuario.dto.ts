import { baseTimeStampFields } from "src/common/base-timestamp.entity";
import { ExcludeType } from "src/helpers/exclude-type.helper";
import { User } from "../users.entity";

export class GetUserDTO extends ExcludeType(User,['password','salt',...baseTimeStampFields]) {}