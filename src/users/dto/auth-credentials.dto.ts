import { ApiProperty } from '@nestjs/swagger';
import { IsString, Matches, MaxLength, MinLength } from 'class-validator';
import { MAX_NAME_LENGTH, MAX_USER_PASSWORD_LENGTH, MIN_NAME_LENGTH, MIN_USER_PASSWORD_LENGTH } from 'src/config/constraints';
import { GetUserDTO } from 'src/users/dto/get-usuario.dto';

export class AuthCredentialsDto {
    @IsString()
    @MinLength(MIN_NAME_LENGTH)
    @MaxLength(MAX_NAME_LENGTH)
    @ApiProperty({
      type: String,
      minLength: MIN_NAME_LENGTH,
      maxLength: MAX_NAME_LENGTH,
    })
    name: string;
  
    @IsString()
    @MinLength(MIN_USER_PASSWORD_LENGTH)
    @MaxLength(MAX_USER_PASSWORD_LENGTH)
    @ApiProperty({
      type: String,
      minLength: MIN_USER_PASSWORD_LENGTH,
      maxLength: MAX_USER_PASSWORD_LENGTH,
    })
    @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
      message: 'senha muito fraca',
    })
    password: string;
}

export class SingInDTO extends GetUserDTO {
    @IsString()
    @ApiProperty({type: String})
    accessToken: string;

}