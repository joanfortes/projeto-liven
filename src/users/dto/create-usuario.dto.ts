
import { baseTimeStampFields } from "src/common/base-timestamp.entity";
import { ExcludeType } from "src/helpers/exclude-type.helper";
import { User } from "../users.entity";

export class CreateUserDTO extends ExcludeType(User,[...baseTimeStampFields,'addresses','salt']) {

}