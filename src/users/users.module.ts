import { forwardRef, Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AddressModule } from 'src/address/address.module';
import { UMA_HORA_EM_SEGUNDOS } from 'src/config/constraints';
import { AuthController } from 'src/users/controllers/auth.controller';
import { JwtStrategy } from 'src/users/jwt-utilities/jwt.strategy';
import { UsersController } from './controllers/users.controller';
import { User } from './users.entity';
import { UsersService } from './users.service';

const passportModule = PassportModule.register({ defaultStrategy: 'jwt' });
@Module({
  imports: [
    passportModule,
    JwtModule.register({
      secret: 'topSecret51',
      signOptions: {
        expiresIn: UMA_HORA_EM_SEGUNDOS,
      },
    }),
    TypeOrmModule.forFeature([User]),
    forwardRef(() => AddressModule),
  ],
  exports:[UsersService,JwtStrategy, passportModule],
  controllers: [UsersController,AuthController],
  providers: [UsersService,JwtStrategy],
})
export class UsersModule {}
