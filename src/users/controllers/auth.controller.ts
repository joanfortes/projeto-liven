import { Body, Controller, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AuthCredentialsDto, SingInDTO } from 'src/users/dto/auth-credentials.dto';
import { CreateUserDTO } from 'src/users/dto/create-usuario.dto';
import { GetUserDTO } from 'src/users/dto/get-usuario.dto';
import { UsersService } from 'src/users/users.service';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private service: UsersService) {}

  @Post('/signup')
  signUp(
    @Body() user: CreateUserDTO
  ): Promise<GetUserDTO> {
    return this.service.signUp(user);
  }

  @Post('/signin')
  signIn(
    @Body() authCredentialsDto: AuthCredentialsDto,
  ): Promise<SingInDTO> {
    return this.service.signIn(authCredentialsDto);
  }
}