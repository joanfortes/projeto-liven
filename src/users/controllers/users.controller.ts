import { Controller, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Crud, CrudController, CrudRequest, Override, ParsedBody, ParsedRequest } from '@nestjsx/crud';
import { CurrentUser } from 'src/helpers/decorators.helper';
import { GetUserDTO } from 'src/users/dto/get-usuario.dto';
import { UpdateUserDTO } from '../dto/update-usuario.dto';
import { User } from '../users.entity';
import { UsersService } from '../users.service';

@Crud({
  model:{
    type: User
  },
  dto: {
    update:UpdateUserDTO, 
  },
  query:{
    join: {
      addresses: {
        eager:true
      }
    }
  },
  routes: {
    only: [
      'getOneBase',
      'updateOneBase',
      'deleteOneBase'
    ]
  }
})
@UseGuards(AuthGuard())
@ApiBearerAuth('accessToken')
@ApiTags('Usuário')
@Controller('/usuario')
export class UsersController implements CrudController<User> {
  constructor(public service: UsersService) {}

  @Override('getOneBase')
  async getOne(
    @ParsedRequest() req: CrudRequest
  ):Promise<GetUserDTO> {
    return this.service.getOneUser(req);
  }

  @Override('updateOneBase')
  async updateOne(
    @ParsedRequest() req: CrudRequest,
    @ParsedBody() dto: UpdateUserDTO
  ) {
    return this.service.updateOneBase(req,dto);
  }

  @Override('deleteOneBase')
  async deleteOne(
    @ParsedRequest() req: CrudRequest,
    @CurrentUser() user: User
  ) {
    return await this.service.deleteOneUser(user,req);
  }
}
