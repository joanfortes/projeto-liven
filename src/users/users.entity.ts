import { ApiProperty } from "@nestjsx/crud/lib/crud";
import * as bcrypt from 'bcrypt';
import { Exclude } from 'class-transformer';
import { IsEmail, IsNotEmpty, Length, Matches } from "class-validator";
import { Address } from "src/address/address.entity";
import { BaseTimeStampEntity } from "src/common/base-timestamp.entity";
import { MAX_NAME_LENGTH, MAX_USER_PASSWORD_LENGTH, MIN_NAME_LENGTH, MIN_USER_PASSWORD_LENGTH } from "src/config/constraints";
import { Column, Entity, JoinColumn, OneToMany, PrimaryGeneratedColumn } from "typeorm";


@Entity('user')
export class User extends BaseTimeStampEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: MAX_NAME_LENGTH })
	@ApiProperty({
		type: String,
		minLength: MIN_NAME_LENGTH,
		maxLength: MAX_NAME_LENGTH,
	})
	@Length(MIN_NAME_LENGTH, MAX_NAME_LENGTH)
  name: string;

  @Column({ type: 'varchar', unique: true })
	@IsEmail()
	@ApiProperty()
  email: string;

  	@Column({type: 'varchar'})
  	@Length(MIN_USER_PASSWORD_LENGTH, MAX_USER_PASSWORD_LENGTH)
  	@IsNotEmpty()
  	@ApiProperty({
	  type: String,
	  minLength: MIN_USER_PASSWORD_LENGTH,
	  maxLength: MAX_USER_PASSWORD_LENGTH,
	})
	@Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
		message: 'senha muito fraca',
	})
	@Exclude()
	password: string;
	
	@Column()
  	@Exclude()
	salt: string;
	
	@OneToMany(() => Address, (address) => address.user, { nullable: true ,cascade:true})
	@JoinColumn([{name:'id_endereco',referencedColumnName: 'id'}])
	@ApiProperty({
		type: () => Address,
		example: {
			id: 'string',
			zipCode: 'string',
			streetName: 'string',
			houseNumber: 'number',
			complement: 'string',
			neighborhood: 'string',
			city: 'string',
			state: 'string',
			country: 'BRA',
		},
	})
	addresses?: Address[];
	
	async validatePassword(password: string): Promise<boolean> {
		const tryPass = await bcrypt.hash(password, this.salt);
		return tryPass === this.password;
	}
}