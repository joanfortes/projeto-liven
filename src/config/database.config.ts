import { ConnectionOptions } from "typeorm";
/*
**   synchronize está false então a cada mudança nas entidades será necessário um migration
*/
const connectionOptions: ConnectionOptions = {
    type: 'postgres',
    host: 'localhost',
    port: 5432,
    username: 'postgres',
    password: 'postgres',
    database: 'teste-liven',
    entities: [`${__dirname}/../**/*.entity.{ts,js}`],
    migrationsRun: true,
    synchronize: false,
    dropSchema: true,
    migrations: ['src/migrations/*{.ts,.js}'],
    cli: {
      migrationsDir: 'src/migrations',
    },
};
  
export = connectionOptions;
