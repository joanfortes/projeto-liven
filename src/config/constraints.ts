
// User
export const MIN_NAME_LENGTH = 7;
export const MAX_NAME_LENGTH = 32;
export const MAX_USER_PASSWORD_LENGTH = 20;
export const MIN_USER_PASSWORD_LENGTH = 8;

// Adress
export const ZIP_CODE_LENGTH = 12;
export const MAX_CITY_NAME_LENGTH = 128;
export const MAX_NEIGHBORHOOD_NAME_LENGTH = 128;
export const MAX_STREET_NAME_LENGTH = 128;
export const MAX_COMPLEMENT_NAME_LENGTH = 128;

// Database
export const MIN_TEXT_LENGTH = 3;

//Tempo
export const UMA_HORA_EM_SEGUNDOS = 60*60