import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const config = new DocumentBuilder()
    .setTitle('Teste Liven')
    .setDescription('Projeto de desafio Liven')
    .setVersion('1.0')
    .addTag('Teste Liven')
    .setContact('Joan Fortes','https://github.com/joanfortes?tab=repositories','joanfortes@hotmail.com')
    .addBearerAuth({type: 'http', scheme: 'Bearer', bearerFormat: 'JWT',in: 'Header',name:'Authorization'},'accessToken')
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);
  await app.listen(3000);
}
bootstrap();
